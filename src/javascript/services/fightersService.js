import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';
  //#endpoint2 = `details/fighter/${id}.json`

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
     // console.log(apiResult);
      
      return apiResult;

    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');
      console.log(apiResult);

      return apiResult;
    } catch (error) {
      throw error;
    } 

    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
  }
}

export const fighterService = new FighterService();
